# Kubernotes

A collection of resources to get started with Kubernetes.

# Trainings/certifications

https://learnk8s.io/ : recommended over the others official trainings listed on kubernetes.io/training\
https://linuxacademy.com/course/kubernetes-security/ : require a subscription\
https://github.com/kelseyhightower/kubernetes-the-hard-way : Kubernetes The Hard Way guides you through bootstrapping a highly available Kubernetes cluster with end-to-end encryption between components and RBAC authentication.
[Kubernetes 101](https://container.training/kube-halfday.yml.html)

# Blogs, articles and others

* [The Ultimate Guide to Kubernetes Security](https://neuvector.com/container-security/kubernetes-security-guide/)
* [How to Secure a Kubernetes Cluster](https://logz.io/blog/how-to-secure-a-kubernetes-cluster/)
* [9 Kubernetes Security Best Practices Everyone Must Follow](https://www.cncf.io/blog/2019/01/14/9-kubernetes-security-best-practices-everyone-must-follow/)
* [Resource center for the O’Reilly book on Kubernetes security](https://kubernetes-security.info/)
* [The Ultimate Guide to Kubernetes Security - How to Deploy Kubernetes with Confidence](https://query.prod.cms.rt.microsoft.com/cms/api/am/binary/RE36AY2)
* [Definitive Guide to Kubernetes Security](https://security.stackrox.com/rs/219-UEH-533/images/Definitive-guide-to-kubernetes-security.pdf)
* [Attacking and Defending Kubernetes: Bust-A-Kube](https://www.inguardians.com/2018/12/12/attacking-and-defending-kubernetes-bust-a-kube-episode-1/)

# Youtube videos
[5 Lessons Learned From Writing Over 300,000 Lines of Infrastructure Code](https://youtu.be/RTEgE2lcyk4): very good video that I recommend watching

# Useful tools
* https://github.com/kubevious/kubevious (looks promising)
* https://github.com/gruntwork-io/terratest : tool to write tests for IaC, works with Kubernetes but not only
* https://gruntwork.io/devops-checklist a devops checklist (I didn't went through it yet)
* https://github.com/google/gke-auditor
* [On Using API](https://kubernetes.io/docs/reference/using-api/)
* [K8S official API](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.19/)
* [Kubesploit](https://www.cyberark.com/resources/all-blog-posts/kubesploit-a-new-offensive-tool-for-testing-containerized-environments)
* [Kubescape](https://github.com/armosec/kubescape): tool for testing if Kubernetes is deployed securely according to multiple frameworks: regulatory, customized company policies and DevSecOps best practices, such as the NSA-CISA and the MITRE ATT&CK®.

# Tutorials

[Kubernetes the hard way](https://github.com/kelseyhightower/kubernetes-the-hard-way)

# Other GitLab resources

Configure's wiki on Kubernetes resources [here](https://gitlab.com/gitlab-org/configure/general/-/wikis/k8s-resources).

# CTF specific to Kubernetes
https://securekubernetes.com/
