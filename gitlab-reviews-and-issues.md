# GitLab's Kubernetes Agent

[GitLab Agent main page](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/tree/master#permissions-within-the-cluster)

[Main issue](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/blob/master/doc/architecture.md#high-level-architecture)

[Agent authentication and authorisation](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/blob/master/doc/identity_and_auth.md)

[GitOps support with the kubernetes Agent](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/blob/master/doc/gitops.md)

Appsec review of GitLab's Kubernetes Agent:
* [1st pass](https://gitlab.com/gitlab-com/gl-security/appsec/appsec-reviews/-/issues/30)
* [2nd pass](https://gitlab.com/gitlab-com/gl-security/appsec/appsec-reviews/-/issues/49)



# Walk through sessions
[With Joern](https://drive.google.com/drive/folders/1Uz0ryd4Sr2Lbp5hVTRuZIqzA-H2HuK8c)

[With Antony](https://drive.google.com/drive/folders/15NpH-EYZgV0ynCYRhXG1nxw8ISXZ9bdm)
